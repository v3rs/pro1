#include <iostream>

using namespace std;

#define PLAYER 'O'
#define OPPONENT 'X'

/**
 * Show instructions, how to pick numbers in board
 *
 * @return Show board layout
 */
void showInstructions() {
    cout << "Wybierz pole do ruchu zgodnie z ponizszym schematem" << endl;
    cout << "\t 1 | 2 | 3 " << endl;
    cout << "\t-----------" << endl;
    cout << "\t 4 | 5 | 6 " << endl;
    cout << "\t-----------" << endl;
    cout << "\t 7 | 8 | 9 " << endl;
}

/**
 * Show board and players moves
 *
 * @param board[][] Board array
 * @return Show board layout with move that players already did
 */
void showBoard(char board[3][3]) {
    cout << "\n";
    for (int row = 0; row < 3; row++) {
        cout << "\t " << board[row][0] << " | " << board[row][1] << " | " << board[row][2] << " " << endl;
        if (row != 2)
            cout << "\t-----------" << endl;
    }
}

/**
 * Elevate actual board
 *
 * @param board[][] Board array
 * @return Value (-10, 0 or 10) that evaluate current board
 */
int evaluate(char board[3][3]) {

    for (int row = 0; row < 3; row++) {
        if (board[row][0] == board[row][1] && board[row][1] == board[row][2]) {
            if (board[row][0] == PLAYER)
                return 10;
            else if (board[row][0] == OPPONENT)
                return -10;
        }
    }

    for (int col = 0; col<3; col++) {
        if (board[0][col] == board[1][col] && board[1][col] == board[2][col]) {
            if (board[0][col] == PLAYER)
                return 10;

            else if (board[0][col] == OPPONENT)
                return -10;
        }
    }

    if (board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
        if (board[0][0] == PLAYER)
            return 10;
        else if (board[0][0] == OPPONENT)
            return -10;
    }

    if (board[0][2] == board[1][1] && board[1][1] == board[2][0]) {
        if (board[0][2] == PLAYER)
            return 10;
        else if (board[0][2] == OPPONENT)
            return -10;
    }

    return 0;
}

/**
 * Check if board has empty space
 *
 * @param board[][] Board array
 * @return True or False
 */
bool isMovesLeft(char board[3][3])
{
    for (int i = 0; i<3; i++)
        for (int j = 0; j<3; j++)
            if (board[i][j]==' ')
                return true;
    return false;
}

/**
 * Implementation of minimax algorithm
 *
 * @param board[][] Board array
 * @param move Describe how many moves has been made
 * @param isMax Describes which strategy is to be chosen by the algorithm (min / max)
 * @return Return best score
 */
int minimax(char board[3][3], int move, bool isMax) {

    int score = evaluate(board);

    if (score == 10)
        return score;
    else if (score == -10)
        return score;

    if (!isMovesLeft(board))
        return 0;

    if (isMax) {
        int best = -1000;

        for (int i = 0; i<3; i++) {
            for (int j = 0; j<3; j++) {
                if (board[i][j] == ' ') {
                    board[i][j] = PLAYER;
                    best = max(best, minimax(board, move + 1, !isMax));

                    board[i][j] = ' ';
                }
            }
        }

        return best;
    } else {
        int best = 1000;

        for (int i = 0; i<3; i++) {
            for (int j = 0; j<3; j++) {
                if (board[i][j] == ' ') {
                    board[i][j] = OPPONENT;
                    best = min(best, minimax(board, move + 1, !isMax));

                    board[i][j] = ' ';
                }
            }
        }
        return best;
    }
}

/**
 * Choose next move form computer
 *
 * @param board[][] Board array
 * @return Return best move to choose
 */
int bestMove(char board[3][3]){
    int bestVal = -1000;
    int x = -1, y = -1;

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            if (board[i][j] == ' ') {
                board[i][j] = PLAYER;

                int moveVal = minimax(board, 0, false);

                board[i][j] = ' ';
                if (moveVal > bestVal) {
                    x = i;
                    y = j;
                    bestVal = moveVal;
                }
            }
        }
    }

    return x * 3 + y;
}

/**
 * Function to manage game
 *
 * @param computerTurn Describes whose turn
 * @return Return best move to choose
 */
void TicTacToe(bool computerTurn) {
	char board[3][3];
    for (auto & i : board) {
        for (char & j : i)
            j = ' ';
    }

    int move = 0, x, y;
	showInstructions();

	while (evaluate(board) != -10 && evaluate(board) != 10 && move != 9) {

		int n;

		if (computerTurn) {
			n = bestMove(board);
			x = n / 3;
			y = n % 3;

			board[x][y] = 'O';
            cout << "Komputer wykonal ruch" << endl;
			showBoard(board);

            move++;
            computerTurn = false;
		} else {
            cout << "\nDo wyboru masz nastepujace pola: ";
			for(int i = 0; i < 3; i++)
				for (int j = 0; j < 3; j++)
					if (board[i][j] == ' ')
                        cout << (i * 3 + j) + 1 << " ";

			cout << "\nPodaj numer pola: ";
			cin >> n;
			n--;
			x = n / 3;
			y = n % 3;

            if (n < 0 || n > 8) {
                cout << "Podano zly numer pola!" << endl;
            } else {
                if(board[x][y] == ' ') {
                    board[x][y] = OPPONENT;
                    showBoard(board);

                    move++;
                    computerTurn = true;
                } else
                    cout << "Wybrano zajete miejse, wybierz wolne pole";
            }
		}
	}

	if (evaluate(board) != -10 && evaluate(board) != 10 && move == 9)
		cout << "Remis" << endl;
	else
        if (!computerTurn)
            cout << "Komputer wygral!" << endl;
        else
            cout << "Wygrales!" << endl;

}

int main() {
	char choice = 't';
    char choiceFirst;

	do {
	 	cout << "Czy chcesz zaczac pierwszy? (t/n): ";
	 	cin >> choiceFirst;

		if (choiceFirst == 'n')
            TicTacToe(true);
		else if (choiceFirst == 't')
            TicTacToe(false);
		else
			cout << "Niepoprawyny wybor" << endl;

        cout << "Chcesz zagrac jeszcze raz? (t/n): ";
        cin >> choice;
	} while(choice == 't');

	return (0);
}